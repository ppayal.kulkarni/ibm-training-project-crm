package TestNGPrograms;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.support.ui.Select;
//import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Project_Activity13 {
    WebDriver driver;
	WebDriverWait wait;
    //Include alwaysRun property on the @BeforeTest
    //to make sure the page opens
    @BeforeClass(alwaysRun = true)
    public void beforeMethod() {
        //Create a new instance of the Firefox driver
        driver = new FirefoxDriver();
            
        //Open the browser
        driver.get("https://alchemy.hguy.co/crm/index.php?action=Login&module=Users");
    }
    
    @Test 
	@Parameters ({"sUsername","password"})
	
	public void login(String sUsername, String password) {
		
		driver.findElement(By.id("user_name")).sendKeys(sUsername);
		driver.findElement(By.id("username_password")).sendKeys(password);
		driver.findElement(By.id("bigbutton")).click();
		
	}
    
    @Test (dependsOnMethods = {"login"})
    public void Product() throws InterruptedException, IOException {
    	wait = new WebDriverWait(driver, 30);
		WebElement button = driver.findElement(By.id("grouptab_5"));
		button.click();
	//	wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Meetings")));
		Thread.sleep(3000);
		WebElement menuUL= driver.findElement(By.xpath("/html/body/div[2]/nav/div/div[2]/ul/li[7]/span[2]/ul"));
		Thread.sleep(3000);
		System.out.println("dropdown:"+menuUL.getText());
    	List<WebElement> menulinks=menuUL.findElements(By.tagName("li"));
    	for (WebElement li : menulinks) {
    	System.out.println(li.getText());
    	
    		if (li.getText().equals("Products")) {
    			
    	     li.click();
    	     break;
    		}
    	}
    	Thread.sleep(4000);
    	
    	driver.findElement(By.xpath("//div[@class = 'actionmenulink' and text()='Create Product']")).click();
    	Thread.sleep(3000);
    	
    	FileInputStream fileName = new FileInputStream("src/TestNGPrograms/Products.xlsx");
    	XSSFWorkbook workbook = new XSSFWorkbook(fileName);
    	XSSFSheet sheet = workbook.getSheetAt(0);
    	int row = sheet.getPhysicalNumberOfRows();
    	int col = sheet.getRow(0).getPhysicalNumberOfCells();
    	
    	System.out.println(row);
    	System.out.println(col);
    	String[][] data = new String[row][col];
  	  	for (int i = 0;i<=row-1;i++) {
  		  
  		  XSSFRow rowdata = sheet.getRow(i);
  		  
		    for(int j=0;j<col;j++) {
		    	
		    	XSSFCell cell = rowdata.getCell(j);
		    	if(cell==null) {
		    		data[i][j] = "";
			       
		    	} else {
		    		String k = rowdata.getCell(j).toString();
			       	data[i][j] = k;
			       
		    	}
  	  	}
  	  	}
  	  System.out.println(data[1][0]);
  	  driver.findElement(By.xpath("//input[@id='name']")).sendKeys(data[1][0]);
  	  
  	  driver.findElement(By.xpath("//input[@id='part_number']")).sendKeys(data[1][1]);
  	  driver.findElement(By.xpath("//input[@id='aos_product_category_name']")).sendKeys(data[1][2]);
  	  driver.findElement(By.xpath("//input[@id='cost']")).sendKeys(data[1][3]);
				  
  	  driver.findElement(By.xpath("//input[@id='price']")).sendKeys(data[1][4]);
			
  	  driver.findElement(By.xpath("//input[@id='contact']")).sendKeys(data[1][5]);
				  
  	  driver.findElement(By.xpath("//textarea[@id='description']")).sendKeys(data[1][6]);
  	  driver.findElement(By.xpath("//input[@id='url']")).sendKeys(data[1][7]);
				  
  	  driver.findElement(By.xpath("//input[@id='SAVE' and @title='Save']")).click();
				  
  	  Thread.sleep(4000);
				
  	  driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='View Products']")).click();
  	  Thread.sleep(4000);
				 
  	  fileName.close();
				  
  	  workbook.close();
				  
  	}
				 
   /* 	WebElement toolbar = driver.findElement(By.xpath("//ul(contains[@class,'recent_h3'])"));
    	List <WebElement> list = toolbar.findElements(By.xpath("//ul/li/a/div[2]"));
    	for (WebElement tool_list: list) {
    		if (tool_list.getText().equals("Import Leads")) {
    			
       	     tool_list.click();
       		}
    	}*/
    	
    	
   @AfterClass(alwaysRun = true)
    public void afterMethod() {
        //Close the browser
       		driver.close();
   }
  	  	
}
    



