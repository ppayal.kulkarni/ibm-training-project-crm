package TestNGPrograms;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;

import java.util.List;

//import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Project_Activity10 {
    WebDriver driver;
	WebDriverWait wait;
    //Include alwaysRun property on the @BeforeTest
    //to make sure the page opens
    @BeforeClass(alwaysRun = true)
    public void beforeMethod() {
        //Create a new instance of the Firefox driver
        driver = new FirefoxDriver();
            
        //Open the browser
        driver.get("https://alchemy.hguy.co/crm/index.php?action=Login&module=Users");
    }
    
    @Test 
	@Parameters ({"sUsername","password"})
	
	public void login(String sUsername, String password) {
		
		driver.findElement(By.id("user_name")).sendKeys(sUsername);
		driver.findElement(By.id("username_password")).sendKeys(password);
		driver.findElement(By.id("bigbutton")).click();
		
	}
    
    @Test (dependsOnMethods = {"login"})
	public void dashlets() throws InterruptedException {
    	
//    	WebElement dashlet = driver.findElement(By.xpath("//table(contains[@class='dashletTable'])/tbody/tr/td/ul/li"));
  //  	System.out.println(dashlet.getAttribute("class"));
  //  	Thread.sleep(3000);
  //  	List<WebElement> dashlist = driver.findElements(By.xpath("//table/tbody/tr/td/ul/li"));
    	Thread.sleep(3000);
    	List<WebElement> dashlist = driver.findElements(By.xpath("//td[contains(@class,'dashletcontainer')]//td[contains(@class,'dashlet-title')]/h3/span[2]"));
    	System.out.println(dashlist.size());
    	for (WebElement li : dashlist) {
    		System.out.println(li.getText());
    		}
    	}
			
        	 
    //	Thread.sleep(3000);
    	
    
    	

   @AfterClass(alwaysRun = true)
    public void afterMethod() {
        //Close the browser
       		driver.close();
   }
}

