package TestNGPrograms;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
public class Project_Activity2to6 {
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement dropdown;
	Actions builder;
	@BeforeTest (alwaysRun = true)
	public void beforemethod() {
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/crm/index.php?action=Login&module=Users");
	}
	
	@Test
	public void getURL() {
		
		System.out.println("URL of the page is: " +driver.findElement(By.linkText("SuiteCRM")).getAttribute("href"));
	}
	
	@Test
	public void getcopyright() {
		System.out.println("First Copyright - footer: " + driver.findElement(By.id("admin_options")).getText());
	}
	
	@Test (dependsOnMethods = {"getURL", "getcopyright"}, priority =0)
	@Parameters ({"sUsername","password"})
	
	public void login(String sUsername, String password) {
		
		driver.findElement(By.id("user_name")).sendKeys(sUsername);
		driver.findElement(By.id("username_password")).sendKeys(password);
		driver.findElement(By.id("bigbutton")).click();
		
	}
	
	@Test (dependsOnMethods = {"login"}, priority =1)
	public void getcolor() {
		
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/nav/div/div[2]/ul/li[1]/a")));
//		System.out.println("title" +driver.getTitle());
		System.out.println("Color of side bar: " +driver.findElement(By.className("sidebar")).getCssValue("color"));
		System.out.println("Color of navigation bar: " +driver.findElement(By.id("toolbar")).getCssValue("color"));
	}
	
	@Test (dependsOnMethods = {"getcolor"}, priority = 2)
	public void getMenu() {
		System.out.println("Is 'Activities' present: "+ driver.findElement(By.id("grouptab_3")).getText());
		
	}
	
	@Test (dependsOnMethods = {"login"}, priority = 3)
	public void getaddinfo() throws InterruptedException {
//		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 10);
		WebElement button = driver.findElement(By.id("grouptab_0"));
		button.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Leads")));
		
		WebElement menuUL= driver.findElement(By.xpath("/html/body/div[2]/nav/div/div[2]/ul/li[2]/span[2]/ul"));
		
		System.out.println("dropdown:"+menuUL.getText());
    	List<WebElement> menulinks=menuUL.findElements(By.tagName("li"));
    	for (WebElement li : menulinks) {
    	System.out.println(li.getText());
    	
    		if (li.getText().equals("Leads")) {
    			
    	     li.click();
    		}
    	}
//		builder= new Actions(driver);
//		builder.click(button).moveByOffset(0,100).build().perform();
//		builder.click();
//		moveToElement(button).pause(Duration.ofSeconds(1)).build().perform();
//				
	//	WebElement leads = driver.findElement(By.linkText("Leads"));
	//	builder.moveByOffset(0,50).build().perform();
		//builder.click();
		//dropdown = driver.findElement(By.className("dropdown-menu"));
	//	Select list = new Select(dropdown);
	//	list.selectByIndex(4);
//		driver.findElement(By.linkText("Leads")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("content")));
	
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[contains(@class,'suitepicon-action-info')]")).click();
		Thread.sleep(2000);
	//	driver.findElement(By.xpath("/html/body/div[2]/div[1]/div[5]/div[2]/span")).getText();
//	    driver.findElement(By.xpath("//span[contains(@id,'2883711f')]/span")).click();
		
	//	System.out.println("Mobile no: " +driver.findElement(By.xpath("//input[starts-with(@value,'2883711f')]/following::span[1]")).getText());
		
		WebElement mobile = driver.findElement(By.xpath("//div[starts-with(@id, 'ui-id-')]//span[@class='phone']"));
		String mobile_num = mobile.getText();
		
		System.out.println("mobile num:"+mobile_num);
	
	}
	
	
//	@AfterTest(alwaysRun = true)
//	public void aftertest() {
//		driver.close();
//	}

}
