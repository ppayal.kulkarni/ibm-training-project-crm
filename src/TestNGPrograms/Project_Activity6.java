package TestNGPrograms;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
//import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Project_Activity6 {
    WebDriver driver;
	WebDriverWait wait;
    //Include alwaysRun property on the @BeforeTest
    //to make sure the page opens
    @BeforeTest(alwaysRun = true)
    public void beforeMethod() {
        //Create a new instance of the Firefox driver
        driver = new FirefoxDriver();
            
        //Open the browser
        driver.get("https://alchemy.hguy.co/crm/index.php?action=Login&module=Users");
    }
    
    @Test 
	@Parameters ({"sUsername","password"})
	
	public void login(String sUsername, String password) {
		
		driver.findElement(By.id("user_name")).sendKeys(sUsername);
		driver.findElement(By.id("username_password")).sendKeys(password);
		driver.findElement(By.id("bigbutton")).click();
		
	}
    
    @Test (dependsOnMethods = {"login"})
	public void getMenu() {
		System.out.println("Is 'Activities' present: "+ driver.findElement(By.id("grouptab_3")).getText());
		
	}
	
   
    @AfterTest(alwaysRun = true)
    public void afterMethod() {
        //Close the browser
       driver.close();
   }

}