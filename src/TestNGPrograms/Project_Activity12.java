package TestNGPrograms;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.util.List;

//import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Project_Activity12 {
    WebDriver driver;
	WebDriverWait wait;
    //Include alwaysRun property on the @BeforeTest
    //to make sure the page opens
    @BeforeClass(alwaysRun = true)
    public void beforeMethod() {
        //Create a new instance of the Firefox driver
        driver = new FirefoxDriver();
            
        //Open the browser
        driver.get("https://alchemy.hguy.co/crm/index.php?action=Login&module=Users");
    }
    
    @Test 
	@Parameters ({"sUsername","password"})
	
	public void login(String sUsername, String password) {
		
		driver.findElement(By.id("user_name")).sendKeys(sUsername);
		driver.findElement(By.id("username_password")).sendKeys(password);
		driver.findElement(By.id("bigbutton")).click();
		
	}
    
    @Test (dependsOnMethods = {"login"})
    public void setMeeting () throws InterruptedException {
    	wait = new WebDriverWait(driver, 30);
		WebElement button = driver.findElement(By.id("grouptab_3"));
		button.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Meetings")));
		Thread.sleep(3000);
		WebElement menuUL= driver.findElement(By.xpath("/html/body/div[2]/nav/div/div[2]/ul/li[5]/span[2]/ul"));
		Thread.sleep(3000);
		System.out.println("dropdown:"+menuUL.getText());
    	List<WebElement> menulinks=menuUL.findElements(By.tagName("li"));
    	for (WebElement li : menulinks) {
    	System.out.println(li.getText());
    	
    		if (li.getText().equals("Meetings")) {
    			
    	     li.click();
    	     break;
    		}
    	}
    	Thread.sleep(3000);
   /* 	WebElement toolbar = driver.findElement(By.xpath("//ul(contains[@class,'recent_h3'])"));
    	List <WebElement> list = toolbar.findElements(By.xpath("//ul/li/a/div[2]"));
    	for (WebElement tool_list: list) {
    		if (tool_list.getText().equals("Import Leads")) {
    			
       	     tool_list.click();
       		}
    	}*/
    	
    	Thread.sleep(2000);
    	
    	driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='Schedule Meeting']")).click();
    	Thread.sleep(2000);
    	driver.findElement(By.xpath("//div/input[@id='name']")).sendKeys("Test Meeting");
    	
    	driver.findElement(By.xpath("//button[@id='date_start_trigger']/span")).click();
    	Thread.sleep(2000);
    	driver.findElement(By.xpath("//table/tbody/tr[4]/td[@id='date_start_trigger_div_t_cell23']/a")).click();
    	driver.findElement(By.xpath("//div/textarea[@id='description']")).sendKeys("This is a placeholder for test meeting");
    	driver.findElement(By.xpath("//table/tbody/tr[1]/td/input[@id='search_first_name']")).sendKeys("QA");
    	driver.findElement(By.xpath("//table/tbody/tr[1]/td/input[@id='invitees_search']")).click();
    	Thread.sleep(2000);
    	driver.findElement(By.xpath("//table/tbody/tr[2]/td/input[@id='invitees_add_1']")).click();
    	driver.findElement(By.xpath("//table/tbody/tr[3]/td/input[@id='invitees_add_2']")).click();
    	driver.findElement(By.xpath("//table/tbody/tr[4]/td/input[@id='invitees_add_3']")).click();
    	driver.findElement(By.xpath("//table/tbody/tr[5]/td/input[@id='invitees_add_4']")).click();
    	driver.findElement(By.xpath("//input[contains(@value,'Send Invites')]")).click();
    	Thread.sleep(4000);
    	driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='View Meetings']")).click();
    	Thread.sleep(2000);
    }


   @AfterClass(alwaysRun = true)
    public void afterMethod() {
        //Close the browser
       		driver.close();
   }
}

