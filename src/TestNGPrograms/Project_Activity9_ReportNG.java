package TestNGPrograms;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.Reporter;

import java.util.List;

//import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Project_Activity9_ReportNG {
    WebDriver driver;
	WebDriverWait wait;
    //Include alwaysRun property on the @BeforeTest
    //to make sure the page opens
    @BeforeTest(alwaysRun = true)
    public void beforeMethod() {
        //Create a new instance of the Firefox driver
        driver = new FirefoxDriver();
            
        //Open the browser
        driver.get("https://alchemy.hguy.co/crm/index.php?action=Login&module=Users");
    }
    
    @Test 
	@Parameters ({"sUsername","password"})
	
	public void login(String sUsername, String password) {
		
		driver.findElement(By.id("user_name")).sendKeys(sUsername);
		Reporter.log(sUsername);
		driver.findElement(By.id("username_password")).sendKeys(password);
		Reporter.log(password);
		driver.findElement(By.id("bigbutton")).click();
		Reporter.log("login successful");
		
		
	}
    
    @Test (dependsOnMethods = {"login"})
	public void gettableinfo() throws InterruptedException {
    	int i;
//		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 30);
		WebElement button = driver.findElement(By.id("grouptab_0"));
		button.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Leads")));
		Thread.sleep(2000);
		WebElement menuUL= driver.findElement(By.xpath("/html/body/div[2]/nav/div/div[2]/ul/li[2]/span[2]/ul"));
		
		System.out.println("dropdown:"+menuUL.getText());
    	List<WebElement> menulinks=menuUL.findElements(By.tagName("li"));
    	for (WebElement li : menulinks) {
    	System.out.println(li.getText());
    	
    		if (li.getText().equals("Leads")) {
    			
    	     li.click();
    	     Reporter.log("selected leads");
    		}
    	}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("content")));
		Thread.sleep(4000);
    
    	List<WebElement> rowNames= driver.findElements(By.xpath("//table[contains(@class, 'table-responsive')]/tbody/tr/td[3]/b/a"));
    	Thread.sleep(3000);
    	
    	Reporter.log("Table loaded successfully");
    	
    	
    	
    	for (i=0;i<10;i++) {
    		System.out.println("Names under column Name: "+ rowNames.get(i).getText());
    		Reporter.log("Data under column Name displayed");
    	}
    	
    	
    /*	for (i=1;i<11;i++) {
    	WebElement rowName= driver.findElement(By.xpath("//table[contains(@class, 'table-responsive')]/tbody/tr[i]/td[3]/b/a"));
    	
    	System.out.println("Name on rows: " +rowName);
    	Thread.sleep(3000);
    	}*/
    	
    	Thread.sleep(3000);
    	List<WebElement> userNames= driver.findElements(By.xpath("//table[contains(@class, 'table-responsive')]/tbody/tr/td[8]/a"));
    	Thread.sleep(3000);
    	
    	
    	
    	for (i=0;i<10;i++) {
    		System.out.println("Usernames under Column Users: "+ userNames.get(i).getText());
    		Reporter.log("Data under column Users displayed");
    	}
    	
    	
    	


  /*  	for (i=1;i<11;i++) {
    		WebElement rowUser= driver.findElement(By.xpath("//table[contains(@class, 'table-responsive')]/tbody/tr[i]/td[8]/a"));
    		Thread.sleep(3000);
    		System.out.println("Usernames under Column User: " +rowUser);
    		Thread.sleep(3000);
    		}
   */
    
    	Thread.sleep(3000);
    
    		
}	
        	 
    //	Thread.sleep(3000);
    	
    
    	

   @AfterTest(alwaysRun = true)
    public void afterMethod() {
        //Close the browser
       		driver.close();
       		Reporter.log("Browser closed successfully");
   }
}

