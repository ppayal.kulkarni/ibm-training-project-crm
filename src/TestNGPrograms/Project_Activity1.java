package TestNGPrograms;

import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
//import org.testng.annotations.AfterTest;
import org.testng.Assert;
//import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Project_Activity1 {
    WebDriver driver;
    //Include alwaysRun property on the @BeforeTest
    //to make sure the page opens
    @BeforeClass(alwaysRun = true)
    public void beforeMethod() {
        //Create a new instance of the Firefox driver
        driver = new FirefoxDriver();
            
        //Open the browser
        driver.get("https://alchemy.hguy.co/crm/index.php?action=Login&module=Users");
    }
    
    @Test
    
    public void gettitle() {
    	
    	String Title = driver.getTitle();
        System.out.println("Title is: " + Title);
    	        
 //     Assert.assertEquals(Title, "SuiteCRM");
        if (Title.equals("SuiteCRM")) {
        	System.out.println("Title matching");
        	driver.close(); 
        }
        		  	
    }
    
   
   
}