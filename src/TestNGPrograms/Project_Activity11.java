package TestNGPrograms;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.util.List;

//import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Project_Activity11 {
    WebDriver driver;
	WebDriverWait wait;
    //Include alwaysRun property on the @BeforeTest
    //to make sure the page opens
    @BeforeClass(alwaysRun = true)
    public void beforeMethod() {
        //Create a new instance of the Firefox driver
        driver = new FirefoxDriver();
            
        //Open the browser
        driver.get("https://alchemy.hguy.co/crm/index.php?action=Login&module=Users");
    }
    
    @Test 
	@Parameters ({"sUsername","password"})
	
	public void login(String sUsername, String password) {
		
		driver.findElement(By.id("user_name")).sendKeys(sUsername);
		driver.findElement(By.id("username_password")).sendKeys(password);
		driver.findElement(By.id("bigbutton")).click();
		
	}
    
    @Test (dependsOnMethods = {"login"})
    public void addLead () throws InterruptedException {
    	wait = new WebDriverWait(driver, 30);
		WebElement button = driver.findElement(By.id("grouptab_0"));
		button.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Leads")));
		Thread.sleep(2000);
		WebElement menuUL= driver.findElement(By.xpath("/html/body/div[2]/nav/div/div[2]/ul/li[2]/span[2]/ul"));
		
		System.out.println("dropdown:"+menuUL.getText());
    	List<WebElement> menulinks=menuUL.findElements(By.tagName("li"));
    	for (WebElement li : menulinks) {
    	System.out.println(li.getText());
    	
    		if (li.getText().equals("Leads")) {
    			
    	     li.click();
    		}
    	}
    	Thread.sleep(3000);
   /* 	WebElement toolbar = driver.findElement(By.xpath("//ul(contains[@class,'recent_h3'])"));
    	List <WebElement> list = toolbar.findElements(By.xpath("//ul/li/a/div[2]"));
    	for (WebElement tool_list: list) {
    		if (tool_list.getText().equals("Import Leads")) {
    			
       	     tool_list.click();
       		}
    	}*/
    	
    	driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='Import Leads']")).click();;
    
    
    WebElement fileSelect = driver.findElement(By.id("userfile"));	
	
	wait.until(ExpectedConditions.visibilityOf(fileSelect));
	try {
		File file = new File("src/Leads.csv");
		WebElement fileSelect1 = driver.findElement(By.xpath("//input[@type='file']"));
		
		fileSelect1.sendKeys(file.getAbsolutePath());
			
		
		
		driver.findElement(By.xpath("//input[@id='gonext' and @class='button']")).click();
		
		
		
		Thread.sleep(4000);
		
		driver.findElement(By.id("gonext")).click();
		
	
		
		Thread.sleep(4000);
		
        WebElement gonext = driver.findElement(By.id("gonext"));
        
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", gonext);
		
		System.out.println("Third Next button is clicked");
		
		Thread.sleep(4000);
		
		WebElement importlead = driver.findElement(By.id("importnow"));
		js.executeScript("arguments[0].click();", importlead);
		
		
		
		Thread.sleep(4000);
		
		String text = driver.findElement(By.className("module-title-text")).getText();
		System.out.println("Text is" +text);
		
		Thread.sleep(2000);
		
		Assert.assertEquals("STEP 5: VIEW IMPORT RESULTS", text);
		
		WebElement result = driver.findElement(By.xpath("//div[@class='screen']/span"));
		/*String result1 = result.getText();
		if(result1.contains("records were created")) {
			System.out.println("Import Successfull" +result1);
				
		} else {
			
			System.out.println("Import Unsuccessfull" +result1);
			
		}*/
		
		
		Thread.sleep(4000);
	}
	catch (Exception e) {
		
		System.out.println(e.getStackTrace());
	}
	
	
	WebElement LeadMenu = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='View Leads']"));
	
	Thread.sleep(2000);
	
	LeadMenu.click();
	
	
	Thread.sleep(2000);
      

}


   @AfterClass(alwaysRun = true)
    public void afterMethod() {
        //Close the browser
       		driver.close();
   }
}

