package SeleniumActivity;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity4_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://www.training-support.net/selenium/target-practice");
		
		System.out.println(driver.getTitle());
		
		System.out.println("Third Header = " + driver.findElement(By.xpath("//h3[@id = 'third-header']")).getText());
//		String fifthHeaderColour = driver.findElement(By.xpath("//h5")).getCssValue("color");
		
	//	System.out.println(fifthHeaderColour);
		String color = driver.findElement(By.xpath("//h5")).getCssValue("color");
		System.out.println(color);
		
		String attribute = driver.findElement(By.xpath("//button[contains(text(),'Violet')]")).getAttribute("class");
		System.out.println(attribute);
	}
	

}
