package SeleniumActivity;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ajax {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        
        driver.get("https://training-support.net/selenium/dynamic-attributes");
        
        System.out.println(driver.getTitle());
        
        driver.findElement(By.xpath("//button[contains(@class,'ui violet button')]")).click();
        
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("ajax-content") ,"HELLO!"));
        
        String ajaxtext = driver.findElement(By.id("ajax-content")).getText();
        System.out.println(ajaxtext);
        
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("ajax-content"), "I'm late!"));
        
        //Get late text and print it
        String lateText = driver.findElement(By.id("ajax-content")).getText();
        System.out.println(lateText);
        
        driver.close();
	}

	
}
