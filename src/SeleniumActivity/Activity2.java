package SeleniumActivity;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://www.training-support.net/");
		String Title = driver.getTitle();
		
		System.out.println("Title is:" + Title);
		
		driver.findElement(By.tagName("a")).click();
		
		driver.getTitle();
		
		System.out.println("New Title is:" + driver.getTitle());
		
		Thread.sleep(2000);
		
		driver.close();
		

		
	}

}
