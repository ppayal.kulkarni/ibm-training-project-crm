package SeleniumActivity;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Actovity7_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://training-support.net/selenium/selects ");
	
		WebElement dropdown = driver.findElement(By.id("multi-select"));
		
		
		Select value = new Select(dropdown);
		
	//	if(value.isMultiple()) {
			
			WebElement chosen = driver.findElement(By.id("multi-value"));
			 value.selectByVisibleText("Javascript");
	            System.out.println(chosen.getText());
	    
	            //Select 'NodeJS' by value
	            value.selectByValue("node");
	            System.out.println(chosen.getText());
		
		
		 //Get all selected options
        List<WebElement> selectedOptions = value.getAllSelectedOptions();
        //Print all selected options
        for(WebElement selectedOption : selectedOptions) {
            System.out.println("Selected option: " + selectedOption.getText());
        }
		//}
		driver.close();
				
        }
	}
	


